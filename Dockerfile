# This file is a template, and might need editing before it works on your project.
FROM python:3.6



WORKDIR /usr/src/app


RUN pip install --no-cache-dir matplotlib

COPY . /usr/src/app



# For some other command
# CMD ["python", "script_code_1.py"]
